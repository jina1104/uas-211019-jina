<?php
 
  include "header.php";
  
?>

<!-- Header -->

<!-- HERO SECTION-->
<section class="py-5 bg-light">
  <div class="container">
    <div class="row px-4 px-lg-5 py-lg-4 align-items-center">
      <div class="col-lg-6">
        <h1 class="h2 text-uppercase mb-0">About</h1>
      </div>
      <div class="col-lg-6 text-lg-right">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-lg-end mb-0 px-0">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">About</li>
            </ol>
          </nav>
      </div>
    </div>
    </div>
</section>

<section class="py-5">
    <div class="container">
  <div class="card mb-3">
  <div class="row no-gutters">
   
    <div class="col-md-12">
      <div class="card-body shadow bg-white rounded">
        <h2 class="card-title text-center">About Us</h2>
        <hr>  
        <p class="card-text">Bukalapak merupakan situs belanja online terpercaya di Indonesia yang menjual beragam produk yang dibutuhkan seluruh masyarakat Indonesia. Seiring berkembangnya teknologi, semakin banyak aktivitas yang dilakukan secara digital, lebih mudah dan praktis, termasuk kegiatan pembelanjaan yang kini semakin marak dilakukan secara digital, baik melalui komputer, laptop, hingga smartphone yang bisa diakses kapan saja dan di mana saja.</p>

        <p class="card-text">Bukalapak hadir sebagai toko online terpercaya dengan sistem konsumen ke konsumen. Hal ini memungkinkan setiap orang untuk menjual dan juga membeli produk dengan mudah secara online. Sarana jual beli online Bukalapak memiliki visi untuk menjadi marketplace nomor satu di Indonesia dengan misi untuk memberdayakan UKM di seluruh penjuru Indonesia. Setiap orang di Indonesia dapat memasarkan produk unggulannya di Bukalapak dengan membuka toko online murah dengan pilihan sistem belanja satuan dan juga grosir.</p>
        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
      </div>
    </div>
  </div>
</div>
</div>


</section>
<!-- footer -->
<?php 
  include "footer.php";
?>